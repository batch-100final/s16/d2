let numberA = null;
let numberB = null;
let operation = null;


//to retrieve an element from the webpage, we can use querySelector
let inputDisplay = document.querySelector("#txt-input-display");
//The document refers to the whole webpage and querySelector is used to select a specific object (HTML Elements)

//the querySelector function takes a string input that is formatted like a CSS selector
//This allows us to get a specific element such as CSS selectors


// document.getElementById()
// document.getElementByClassName()
// document.getElementByTagname()

let btnNumbers = document.querySelectorAll(".btn-numbers");

//btnAdd
//btnSubtract
//btnMultiply
//btnDivide
//btnEqual
//btnDecimal
//btnClearAll
//btnBackspace

let btnAdd = document.querySelector("#btn-add");
let btnSubtract = document.querySelector("#btn-subtract");
let btnMultiply = document.querySelector("#btn-multiply");
let btnDivide = document.querySelector("#btn-divide");

let btnEqual = document.querySelector("#btn-equal");
let btnDecimal = document.querySelector("#btn-decimal");

let btnClearAll = document.querySelector("#btn-clear-all");
let btnBackspace = document.querySelector("#btn-backspace");

btnNumbers.forEach(function(btnNumber){
	btnNumber.onclick = () => {
		inputDisplay.value += btnNumber.textContent;
	}
})
/////////////////////-- add   -- ///////////////////////
btnAdd.onclick = () => {
	if (numberA == null) {
		numberA = Number(inputDisplay.value);
		operation = "addition";
		inputDisplay.value = null;
	}
	else if (numberB == null){
		numberB = Number(inputDisplay.value);
		numberA = numberA + numberB;
		operation = "addition";
		numberB.value = null;
		inputDisplay.value = null;
	}
}
/////////////////////-- subtract -- ///////////////////////
btnSubtract.onclick = () => {
	if (numberA == null) {
		numberA = Number(inputDisplay.value);
		operation = "subtraction";
		inputDisplay.value = null;
	}
	else if (numberB == null){
		numberB = Number(inputDisplay.value);
		numberA = numberA - numberB;
		operation = "subtraction";
		numberB.value = null;
		inputDisplay.value = null;
	}
}

/////////////////////-- multiply -- ///////////////////////
btnMultiply.onclick = () => {
	if (numberA == null) {
		numberA = Number(inputDisplay.value);
		operation = "multiplication";
		inputDisplay.value = null;
	}
	else if (numberB == null){
		numberB = Number(inputDisplay.value);
		numberA = numberA * numberB;
		operation = "multiplication";
		numberB.value = null;
		inputDisplay.value = null;
	}
}

/////////////////////-- divide -- ///////////////////////
btnDivide.onclick = () => {
	if (numberA == null) {
		numberA = Number(inputDisplay.value);
		operation = "division";
		inputDisplay.value = null;
	}
	else if (numberB == null){
		numberB = Number(inputDisplay.value);
		numberA = numberA / numberB;
		operation = "division";
		numberB.value = null;
		inputDisplay.value = null;
	}
}

///////////////////-- clear all --/////////////////////////
//null
btnClearAll.onclick = () => {
	numberA = null;
	numberB = null;
	operation = null;
	inputDisplay.value = null;
}

/////////////////////--  backspace     -- ///////////////////////
//slice delete lang ng dulo (0 <- start , -1 <- end)
btnBackspace.onclick = () => {
	inputDisplay.value = inputDisplay.value.slice(0, -1)
}

/////////////////////--  decimal  -- ///////////////////////
btnDecimal.onclick = () => {
	if (!inputDisplay.value.includes(".")){
		inputDisplay.value = inputDisplay.value + btnDecimal.textContent;
	}
}

/////////////////////--  equal  -- ///////////////////////
btnEqual.onclick = () => {
	if (numberB == null && inputDisplay.value !== ""){
		numberB = inputDisplay.value;
	}
	if (operation == 'addition') {
		inputDisplay.value = Number(numberA) + Number(numberB);
	}
	if 	(operation == 'subtraction') {
		inputDisplay.value = Number(numberA) - Number(numberB);
	}
	if 	(operation == 'multiplication') {
		inputDisplay.value = Number(numberA) * Number(numberB);
	}
	if 	(operation == 'division') {
		inputDisplay.value = Number(numberA) / Number(numberB);
	}	
}


const fName = document.querySelector("#fName");
const lName = document.querySelector("#lName");
log = document.querySelector("#firstName");
log2= document.querySelector("#lastName");

fName.addEventListener("input", firstName);
lName.addEventListener("input", lastName);

function firstName(f) {
	log.textContent = f.target.value;
}

function lastName(l) {
	log2.textContent = l.target.value;
}